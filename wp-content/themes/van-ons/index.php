<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>De Laatste Downer (EO)</title>
    <link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/app.css">
  </head>
  <body>
<div class="responsive-slick">

        <?php
            $args = array( 'post_type' => 'eo_poll');
            $loop = new WP_Query( $args );
            $i=0;
            while ( $loop->have_posts() ) :
                $i++;
                $loop->the_post();
                $background_image = get_post_meta(get_the_ID(),'background_image', true);
        ?>

        <div style="background-image:url(<?php echo $background_image; ?>)" data-qid="<?php echo get_the_ID(); ?>" class="eo-slide" id="slide_<?php echo get_the_ID(); ?>">
          <div class="row">
            <div class="small-12 columns small-centered">
                <h1><?php the_title(); ?></h1>
                <p class="header_<?php echo get_the_ID(); ?>">Verschuif om je mening te geven.</p>
                <div class="eo-slider">
                    <div class="eo-handle">
                        <div class="responsive-slider"></div>
                    </div>
                    <div class="eo-bar">
                        <div class="eo-bar-rail"></div>

                        <?php
                        $group = get_post_meta( get_the_ID(), 'antwoorden', true );
                        if($group):
                            $ii = 0;
                            foreach($group as $item): $ii++;
                        ?>
                        <div class="eo-spot eo-spot_<?php echo $ii; ?> <?php if(count($group)==$ii) echo "last"; ?>"><div class="eo-tooltip balloon_<?php echo get_the_ID(); ?>_<?php echo $ii; ?>"><h2><?=$item['antwoord_title']; ?></h2><p> <?=$item['antwoord_text']; ?></p></div></div>
                        <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                </div>
                <p class="footer_<?php echo get_the_ID(); ?>">Bevestig je keus en kijk wat andere mensen hebben gekozen.</p>
                <div class="text-centered eo-button  eo-button_<?php echo get_the_ID(); ?>">
                    <input type="submit" disabled="disabled" class="view-results button" data-qid="<?php echo get_the_ID(); ?>" value="Bekijk">
                </div>
            </div>
          </div>
      </div><!--/.slide-->
      <?php
          endwhile;
      ?>


      <div style="background-image:url(http://vanons.overman.io/wp-content/uploads/2016/04/back3.jpg)" class="">
          <div class="row">
            <div class="small-7 columns small-centered eo-slide-last">
                <h1>60% breekt de zwangerschap af</h1>
                <p class="">Binnenkort wordt de NIPT waarschijnelijk breed aangeboden. Dit betekend dat een grotere groep vrouwen in vroeg stadium van hun zwangerschap kan zien of hun kindje downsyndroom heeft. De verwachting is dat steeds meer gezinnen dan voor de keus zullen staan om een zwangerschap wel of niet af te breken.</p>
                <p class=""><b>Wat zou jij doen?</b> Heb je behoefte om verder te praten over dit onderwerp? Chat dan gratis en anoniem met Siriz. Of scroll naar beneden en lees meer informatie.</p>
                <a href="/" class="button">Begin opnieuw</a>
                <a href="#" class="button">Meer info</a>

                <h2>Kies een filmpje en deel met je vrienden</h2>

                <div class="films">
                    <div class="film" style="background-image:url(http://vanons.overman.io/wp-content/uploads/2016/04/back3.jpg)"></div>
                    <div class="film" style="background-image:url(http://vanons.overman.io/wp-content/uploads/2016/04/back2.jpg)"></div>
                    <div class="film" style="background-image:url(http://vanons.overman.io/wp-content/uploads/2016/04/back3.jpg)"></div>
                    <div class="film" style="background-image:url(http://vanons.overman.io/wp-content/uploads/2016/04/back1.jpg)"></div>
                    <div class="film" style="background-image:url(http://vanons.overman.io/wp-content/uploads/2016/04/back3.jpg)"></div>
                </div>

            </div>
          </div>
      </div><!--/.slide-->


    </div><!--/.responsive-slick-->

    <script src="<?php bloginfo('template_url'); ?>/bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/bower_components/what-input/what-input.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/bower_components/foundation-sites/dist/foundation.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/poll.js"></script>

  </body>
</html>
