// css media breakpoints.
var media = {
    small: 0,
    medium: 640,
    large: 1024
}


// Poll functionality (slider, carousel, submition)
function Poll()
{

    this.hollow_spot_placed = [];

    // Get all question ids
    this.question_ids = [];
    var self = this;
    $.each($('.eo-slide'), function( index, value ) {
        self.question_ids.push($(this).data('qid'));
    });

    // Set all events
    this.setEvents();

    // Create carousel
    $('.responsive-slick').slick({
        draggable: false,
        adaptiveHeight: true
    });
}

Poll.prototype.setEvents = function()
{

    // Create horizontal or vertical slider depending on window width
    $(window).resize($.proxy(function(){

        var window_width = $(window).width();
        var window_height = $(window).height();

        // Remove all sliders
        $('.responsive-slider').empty();

        // Create the slider
        if(window_width<=media.medium){
            this.createSlider('vertical');
        }else{
            this.createSlider('horizontal');
        }

        // Adapt slide width to window
        $('.responsive-slick, .slick-list').height(window.innerHeight);
        $('.responsive-slick').width(window_width);
        // $('.orbit, .orbit-container, .orbid-slide').width(window_width);

        // Init the foundation slider plugin
        $('.slider').foundation();

    },this));


    // Init the sliders on page load
    $(function(){ $(window).trigger('resize')});


    // Question submittion
    $('input.view-results').on('click', function(){

        var qid = $(this).data('qid');
        var choice = $('#slide_'+qid).find('.slider-handle').attr('aria-valuenow');

        $.ajax({
            url: "/wp-admin/admin-ajax.php",
            type:'POST',
            data: "action=vote&qid="+qid+"&choice="+choice,
            success: function(data){

                var json = $.parseJSON(data);

                // Remove the slider handle
                $('#slide_'+qid).find('.responsive-slider').fadeOut();

                // Update items
                for(i=0;i<json.updates.length;i++){
                    $(json.updates[i].key).html(json.updates[i].content);
                }

            }
        });


    })


}



Poll.prototype.createSlider = function(orientation)
{

    var isEven  = function(x) { return !( x & 1 ); };

    var self = this;
    $.each(this.question_ids, function( index, value ) {

        var steps = $('#slide_'+value).find('.eo-spot').length;
        var innitial_start = parseInt(steps/2);

        var even = isEven($('#slide_'+value).find('.eo-spot').length);

        // Add the innitial unchoosable spot
        if(even && !self.hollow_spot_placed[value]) {
            $('#slide_'+value).find('.eo-spot_'+innitial_start).after('<div class="eo-spot-hollow"></div>');
            self.hollow_spot_placed[value] = true;
        }else if(!even) {
            steps = steps -1;
        }



        // Create foundation slider
        switch(orientation){
            case 'horizontal':
                var slider = '<div class="slider" data-slider data-initial-start="'+innitial_start+'" data-end="'+(steps)+'" data-step="1" data-vertical="false">\
                  <span class="slider-handle"  data-slider-handle role="slider" tabindex="1" aria-controls="sliderOutput2"></span>\
                  <span class="slider-fill" data-slider-fill></span>\
                </div>\
                <input type="hidden" name="orientation" value="horizontal">\
                <input type="hidden" id="sliderOutput2" >';

                $('#slide_'+value).find('.responsive-slider').html(slider);

                // set slider width to spot bar width
                $('#slide_'+value).find('.eo-slider').width($('#slide_'+value).find('.eo-bar').width()+35);
                $('.slider, .responsive-slider, .eo-handle').css('width','100%');
                $('.eo-handle').height(50);
                break;

            case 'vertical':
                var slider = '<div class="slider vertical" data-slider data-initial-start="'+innitial_start+'" data-end="'+(steps)+'" data-step="1" data-vertical="true">\
                  <span class="slider-handle"  data-slider-handle role="slider" tabindex="1" aria-controls="sliderOutput2"></span>\
                  <span class="slider-fill" data-slider-fill></span>\
                </div>\
                <input type="hidden" name="orientation" value="vertical">\
                <input type="hidden" id="sliderOutput2" >';

                $('.eo-handle').width(50);

                $('#slide_'+value).find('.responsive-slider').html(slider);
                $('#slide_'+value).find('.slider').css('height',($('#slide_'+value).find('.eo-bar').height()+15)+'px');
                break;
        }


        // Enable the button if a valid answer is choosen
        if(even){
            $('#slide_'+value).find('.responsive-slider').on('moved.zf.slider', function(){
                 if($('#slide_'+value).find('.slider-handle').attr('aria-valuenow')!=innitial_start) {
                     $('#slide_'+value).find('.view-results').prop('disabled', false);
                 }else{
                     $('#slide_'+value).find('.view-results').prop('disabled', true);
                 }
            });
        }else{

            // Sliders with no blind spot alway have the button activated
            $('#slide_'+value).find('.view-results').prop('disabled', false);
        }

    });

}

// Init the foundation framework
$(document).foundation();

var poll = new Poll;
