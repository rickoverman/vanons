<?php


// Set monthnames to dutch
setlocale(LC_ALL, 'nl_NL');

/**
 * This module setup makes use of the CMB2 metabox creation library
 * @link     https://github.com/WebDevStudios/CMB2
 */
if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}
 

// Autoload all modules located in the `modules` folder.
include_once(dirname(__FILE__).'/modules/autoload.php');
