<?php

$modules = array_filter(glob(dirname(__FILE__).'/*'), 'is_dir');

foreach ($modules as $module) {
    if(file_exists("{$module}/init.php")) include("{$module}/init.php");
}
