<?php

class Poll {

	// Custom post type settings
	static $wp;
	static $name = 'eo_poll';
	static $singular_name = 'Vraag';
	static $menu_name = 'Poll';
	static $name_admin_bar = 'Poll';
	static $label = 'Poll';
	static $description = 'Poll Vragen';
    static $cmb;

	// Lets begin!
	public function setup($wp) {

		self::$wp = $wp;

		# Set custom image sizes used by this post type
		add_image_size( 'poll_small', 800, 800 );
		add_image_size( 'poll_big', 1800, 1800 );

		# Add sizes to the admin selection
		add_filter( 'image_size_names_choose', [__CLASS__,'register_custom_image_sizes'] );

		// Create the custom post type & meta boxes
		add_action( 'init', [__CLASS__, 'create_post_type'], 0 );
        add_action( 'cmb2_admin_init', [__CLASS__,'create_meta_boxes'] );

		// Register ajax functionalities
		add_action('wp_ajax_vote', [__CLASS__,'wp_vote']);
		add_action('wp_ajax_nopriv_vote', [__CLASS__,'wp_vote']);


	}

	// The infinite scroll ajax output
	public static function wp_vote(){

		$qid = (int) $_POST['qid'];
		$choice = (int) $_POST['choice'];


		// Get choices
		$choices = get_post_meta($qid,'antwoorden', true);

		// Adjust choice index when even
		if (count($choices) % 2 == 0 && $choice>(count($choices)/2)) {
		  $choice = $choice - 1;
		}


		$votes_total = 0;
		$choices_new = [];
		foreach ($choices as $i => $c) {
			 if(intval($i)==$choice)  $c['votes'] = $c['votes'] + 1;
			 $c_new[] = $c;
			 $votes_total = $votes_total + intval($c['votes']);
		}

		// store the vote in de database
        update_post_meta($qid,'antwoorden', $c_new);

		// create json data
		$data = [];
		foreach ($c_new as $i => $c) {
			$perc =  round((100/$votes_total) * intval($c['votes']));
			$data[] = array('key'=>".balloon_{$qid}_".($i+1),"content"=>"<h2>{$perc}%</h2><p>{$c['antwoord_title']}</p>");

		}

		$data[] = array("key"=>".header_{$qid}","content"=>"Dit denken andere bezoekers");
		$data[] = array("key"=>".footer_{$qid}","content"=>"&nbsp;");
		$data[] = array("key"=>".eo-button_{$qid}","content"=>"<input type=\"submit\" onclick=\"$('.responsive-slick').slick('slickNext')\" class=\"button\"  value=\"Ga verder\">");


		echo json_encode(array("updates"=>$data));

	    exit;
	}


	# Add custom image sizes to admin
	public static function register_custom_image_sizes( $sizes ) {
		return array_merge( $sizes, array(
			'poll_small' => __( 'Poll foto klein' ),
			'poll_big' => __( 'Poll foto groot' ),
		));
	}


	// Register Custom Post Type
	public static function create_post_type() {

		$labels = array(
			'name'                  => _x( self::$name, 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( self::$singular_name, 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( self::$menu_name, 'text_domain' ),
			'name_admin_bar'        => __( self::$name_admin_bar, 'text_domain' ),
			'archives'              => __( 'Item Archives', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
			'all_items'             => __( 'All Items', 'text_domain' ),
			'add_new_item'          => __( 'Add New Item', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New Item', 'text_domain' ),
			'edit_item'             => __( 'Edit Item', 'text_domain' ),
			'update_item'           => __( 'Update Item', 'text_domain' ),
			'view_item'             => __( 'View Item', 'text_domain' ),
			'search_items'          => __( 'Search Item', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Items list', 'text_domain' ),
			'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( self::$label, 'text_domain' ),
			'description'           => __( self::$description, 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array('title'),
	#		'taxonomies'            => array( 'category' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'rewrite'				=> array('slug' => 'poll')
		);
		register_post_type(self::$name, $args );
		flush_rewrite_rules();
		//add_post_type_support(self::$name, 'thumbnail');

	}


    public static function create_meta_boxes()
    {

        self::$cmb = new_cmb2_box( array(
            'id'            => 'metabox'.self::$name,
            'title'         => __( 'Extra informatie', 'cmb2' ),
            'object_types'  => array(self::$name, ), // Post type
            // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
             'context'    => 'normal',
            // 'priority'   => 'high',
            // 'show_names' => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // true to keep the metabox closed by default
        ) );


    	self::$cmb->add_field( array(
    		'name' => __( 'Achtergrond foto', 'cmb2' ),
    		'desc' => __( 'Achtergrond foto voor deze vraag.', 'cmb2' ),
    		'id'   =>  'background_image',
    		'type' => 'file',
    	) );


        /**
         * Repeatable Field Groups
         */
        $cmb_group = new_cmb2_box( array(
            'id'           =>  'metabox2'.self::$name,
            'title'        => __( 'Antwoorden', 'cmb2' ),
            'object_types' =>  array(self::$name, ), // Post type

        ) );

        // $group_field_id is the field id string, so in this case: $prefix . 'demo'
        $group_field_id = $cmb_group->add_field( array(
            'id'          => 'antwoorden',
            'type'        => 'group',
            'description' => __( 'Antwoord', 'cmb2' ),
            'options'     => array(
                'group_title'   => __( 'Antwoord {#}', 'cmb2' ), // {#} gets replaced by row number
                'add_button'    => __( 'Nieuw antwoord toevoegen', 'cmb2' ),
                'remove_button' => __( 'Antwoord verwijderen', 'cmb2' ),
                'sortable'      => true, // beta
                // 'closed'     => true, // true to have the groups closed by default
            ),
        ) );


        /**
         * Group fields works the same, except ids only need
         * to be unique to the group. Prefix is not needed.
         *
         * The parent field's id needs to be passed as the first argument.
         */
        $cmb_group->add_group_field( $group_field_id, array(
            'name'       => __( 'Antwoord titel', 'cmb2' ),
            'id'         => 'antwoord_title',
            'type'       => 'text',
            // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
        ) );

		/**
         * Group fields works the same, except ids only need
         * to be unique to the group. Prefix is not needed.
         *
         * The parent field's id needs to be passed as the first argument.
         */
        $cmb_group->add_group_field( $group_field_id, array(
            'name'       => __( 'Antwoord text', 'cmb2' ),
            'id'         => 'antwoord_text',
            'type'       => 'text',
            // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
        ) );

		$cmb_group->add_group_field( $group_field_id, array(
            'name'       => __( 'Aantal stemmen', 'cmb2' ),
            'id'         => 'votes',
            'type'       => 'text',
            // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
        ) );


    }


    public function register_js()
    {

    }


    public function register_css()
    {

    }


}

// Create  custom post type with custom meta boxes and  ajax requests
Poll::setup($wpdb);
