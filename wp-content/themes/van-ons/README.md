 # De Laatste Downer Poll

 Dit document beschrijft de verschillende onderdelen van het systeem.


 ## Gebruikte frameworks en libraries

    * WordPress 4.5
        - CMB2
        - Thema Modules

    * Foundation 6
        - Sass
        - Slider plugin

    * Slick carousel


## WordPress

Alle custom wordpress functionaliteit is te vinden in de `inc/` folder van het thema.

In deze folder is alles opgezet als soort van `modules`, dit maakt het mogelijk  om gemakkelijk extra functionaliteit te implementeren, verwijderen of vervangen, simpelweg door de specefieke module folder in `inc/modules/` te plaatsen of te verwijderen.

In dit project is er maar gebruik gemaakt van 1 module genaamd `poll`.

De module `poll` maakt gebruik van de [CMB2 library](https://github.com/WebDevStudios/CMB2) dat ook toegankelijk is voor eventuele andere modules.


## Foundation 6

Voor de front-end is er zoveel mogelijk gebruik gemaakt van het [Foundation 6](http://foundation.zurb.com/sites.html) framework en de bijbehoorende plugins. Alle css is dan ook gecompiled vanuit Sass bestanden. Deze Sass bestanden kan men vinden in de `scss/` folder.


## Slick carousel

Ondanks dat Foundation 6 een erg goed framework is is het ook erg te merken dat het framework nog maar net uit is en in de kinderschoenen staat. Dit is vooral te merken als je werkt met de Javascript plugins als Orbit (carousel) waar nog maar weinig functionaliteit in is geimplementeerd.

Hierdoor heb ik Orbit moeten laten vallen en heb daarvoor in de plaats de [Slick carousel](http://kenwheeler.github.io/slick/) gebruikt waarop Orbit van Foundation trouwens op is gebaseerd.
